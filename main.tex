\documentclass[twocolumn]{article}
\usepackage[margin=0.5in]{geometry}
\usepackage{times}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage[sorting=none]{biblatex}
\addbibresource{citations.bib}


\lstdefinestyle{mystyle}{
    commentstyle=\color{gray},
    keywordstyle=\color{blue},
    numberstyle=\color{gray}\small\ttfamily,
    stringstyle=\color{orange},
    basicstyle=\small\ttfamily,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    frame=tlrb
}


\title{Exploring Spectre Attacks on SonicBOOM}
\author{Derek Rodriguez}
\date{\today}

\begin{document}


\maketitle 
\begin{center}
    \textbf{Abstract} 
\end{center}
This report presents a detailed analysis of a Spectre v1 attack (bounds check bypass) as originally in
Kocher et al~\cite{kocher_spectre_2018} running on a simple configuration of
SonicBOOM~\cite{zhao2020sonicboom} using some adaptations originally presented
by Gonzalez et al~\cite{gonzalez_spectrum_2018}. This report presents detailed
visualizations of the branch predictor behavior and L1 cache behavior as
captured through simulated execution in Verilator, followed by an analysis of
the attack's results. 

\section{Introduction} 
Semiconductor manufacturers continuously seek to produce newer designs by
increasing the number of transistors they can place on a silicon wafer. This
allows computer architects to incorporate more performance optimizations in
their designs, empowering software to solve problems and perform tasks in less
time. These optimizations come at the cost of making it more difficult to
verify~\cite{eetimes_eetimes_2004}. In this work, we focus on one specific case
where incorrect behavior results in the grave consequence of breaking the
security guarantees from the hardware all the way up to the application level.
This report seeks to illustrate the inner workings of these attacks on
SonicBOOM\cite{zhao2020sonicboom}, the latest iteration of a popular
out-of-order microarchitecture targeting the RISC-V ISA. 

\section{Background}

\subsection{Speculative Execution} 
One way of improving the instruction throughput in microprocessors is through
speculating on the outcome of memory-bound branches. For example, consider the
following code listing:
\begin{lstlisting}[style=mystyle,language=c]
    void delegator(int *large_array, size_t len){
        if (large_array[len-1] != 0) {
            do_full(large_array, len);
        } else {
            do_nonfull(large_array, len);
        }
    }
\end{lstlisting}

In this example, \texttt{large\_array[len-1]} is a piece of data which may not
be located on the cache and thus may take several cycles to retrieve. Instead of
stalling the pipeline, we instead predict the outcome of the branch and start
executing speculative instructions while the branch resolves. If the prediction
was correct, then the latency of the memory access was hidden. If the prediction
is incorrect then the pipeline is flushed and the correct execution path is
taken, incurring a minimal performance penalty. While speculative execution can
provide significant increases in instruction
throughput~\cite{gonzalez_speculative_1997}, it is unfortunately the case that
these speculated instructions can read arbitrary memory which can be made
visible through microarchitectural side channels, as has been proven to be
possible on billions of CPUs in many of our everyday
electronics~\cite{kocher_spectre_2018}. Microarchitectural side channels have
been a topic of concern at least since \cite{percival_cache_nodate}, it is only
in the past few years \cite{lipp_meltdown:_nodate,kocher_spectre_2018}, that we
have seen them used in this more dangerous form

\subsection{Spectre attacks}
Spectre attacks are an approach to leaking secret data across both intraprocess
and interprocess boundaries by creating speculative reads which bypass privilege
checks, and then recovering that data by exploiting the implementation details
of a certain processor's microarchitecture (e.g., cache timings, AVX registers~\cite{schwarz_netspectre_2019},
execution ports~\cite{bhattacharyya_smotherspectre_2019}, etc).

In this work, we focus specifically on Spectre v1, which consists of the
following steps: 
\begin{enumerate}
    \item Flood the cache with attacker-controlled junk data.
    \item Train the branch predictor to always take a branch, speculating a code
    path which reads from memory. 
    \item Use the trained predictor to misspeculate a memory read from an arbitrary source.
    \item Write the data to cache, and then recover the data using a side
    channel, such as a timing channel.
\end{enumerate}

\subsection{SonicBOOM} 
SonicBOOM~\cite{zhao2020sonicboom} is the third iteration of the Berkeley
Out-of-Order Machine~\cite{asanovic2015berkeley}. It serves as a good target for
this research due to its open source implementation in
Chipyard~\cite{amid2020chipyard}, allowing for much deeper inspection than a
typical commercially available out-of-order microarchitecture while still being
competitive in regards to performance. Furthermore, BOOM can be easily
configured with both a GShare~\cite{predictors1993combining} or an
L-TAGE~\cite{seznec2007256} branch predictor.

\begin{figure*}
    \includegraphics[width=\textwidth]{fig/memreads.png}
    \caption{A portion of the cache activity traced from the simulation, with
    the malicious speculated reads in blue.}
\end{figure*}

\subsection{RISC-V}
RISC-V is an instruction set architecture noted for its zero-cost licensing and
a notable amount of available open source implementations. The RISC-V ISA has its instruction set partitioned 
according to the level of functionality different computer architects need to support. SonicBOOM supports 
the entirety of the RV64GC, which encompasses extensions such as atomics, floating point instructions, 
compressed instructions~\cite{waterman2011improving}, and all of the system-level functionality that would be 
required to run something such as the Linux kernel.

\section{Reproducing Spectre v1 on SonicBOOM}
The methodology for performing attack is adapted from by Gonzalez et
al~\cite{gonzalez_spectrum_2018}, which used the previous iteration of the
microarchitecture, BOOMv2~\cite{celio2017boomv2}. We use the sample
proof-of-concept code for the Spectre v1 attack sourced from
\href{https://github.com/riscv-boom/boom-attacks}{https://github.com/riscv-boom/boom-attacks}.
To collect our results, we elaborate the build of SonicBOOM included in the
1.5.0 release of Chipyard using the modified \texttt{SmallBoomConfig}
parameters. To simulate execution, the source code written in Chipyard DSL is
compiled and then simulated to Verilator. Cache data is extracted by embedding
logging statements in the into the Chipyard source, with a snippet visible in
Figure 1. Cycle counts are extracted from the inside the attack code, and thus
are subject to the overhead of execution, but are also more reflective of the
latency that would be experienced in attack occuring out in the wild.

Both branch predictors were tested using this methodology, however the TAGE-L
predictor was much less prone to leaking data. This is quite interesting, as the
same attack was tested successfully on a Ryzen 3700X, which has a similar TAGE
predictor~\cite{suggs_amd_2020}. Adapting the attack to work with SonicBOOM's TAGE-L
predictor is left for future work. For the Gshare predictor, we obtain the results 
which can be seen in Table 1.
\newpage

\begin{table}[]
    \begin{tabular}{|l|l|l|}
    \hline
    \textbf{Spectre v1 Results} & Our results & Gonzalez et al \\ \hline
    Cycles per Byte             & 1043950     & 884485         \\ \hline
    Bandwidth @ 100MHz          & 95.7 B/s    & 113 B/s        \\ \hline
    Bandwidth @ 3.2GHz          & 3065 B/s    & 3618 B/s       \\ \hline
    Relative difference         & -15.275\%    & 0.000\%        \\ \hline
    \end{tabular}
    \caption{ These results are compared against the work of Gonzalez et
    al~\cite{gonzalez_spectrum_2018} as this the only known prior work at the
    time of writing.}
\end{table}

\section{Conclusion}

It is of note that the results in Table 1 differ from prior work for reasons
beyond just microarchitecture. Gonzalez et al leverage
FireSim~\cite{karandikar2018firesim}, an FPGA-accelerated simulation platform,
to collect their results. Furthermore, it is clear that the proof-of-concept
code isn't necessarily optimized for throughput, as there are likely several
cycles which could be saved by avoiding tasks such as printing to console, or
shortening the pipeline stall introduced by the floating point instructions in
the victim function. Furthermore, the changes in the L1 cache, as well as the
Load Store Unit likely are not intended to perform well for these malicious
attack patterns.

In this work, we've demonstrated that Spectre v1 is still feasible to execute on
the latest iteration of the BOOM microarchitecture. While these results aren't
as performant as the results demonstrated in prior work, the attack as
demonstrated is more than capable of leaking something such as an AES key from
cache in a reasonable amount of time. Topics of future interest include better simulation
introspection as well as adapting the attack (or other Spectre variants) to work better with
the L-TAGE predictor which was added as an option in SonicBOOM.
\newpage
\printbibliography

\end{document}

